TLDD
----

A program similar to [ldd(1)][ldd] but showing the output as a tree, so you can
see where a particular dependency comes from.

Requires a C++11 compiler and the header [`pstream.h`][pstream.h] from
http://pstreams.sf.net/ (see the `INSTALL` file for more information).

## Example

Here is the output of `tldd` inspecting itself:

    $ ./tldd ./tldd
    ./tldd
    └─libstdc++.so.6 => /lib64/libstdc++.so.6 (0x00007fa1b7400000)
      ├─libm.so.6 => /lib64/libm.so.6 (0x00007fa1b7324000)
      │ └─libc.so.6 => /lib64/libc.so.6 (0x00007fa1b7000000)
      │   └─ld-linux-x86-64.so.2 => /lib64/ld-linux-x86-64.so.2 (0x00007fa1b7716000)
      └─libgcc_s.so.1 => /lib64/libgcc_s.so.1 (0x00007fa1b76ca000)

Use the `--full` option to show repeated dependencies that occur more than once:

    ./tldd
    ├─libstdc++.so.6 => /lib64/libstdc++.so.6 (0x00007f2d09e00000)
    │ ├─libm.so.6 => /lib64/libm.so.6 (0x00007f2d0a096000)
    │ │ ├─libc.so.6 => /lib64/libc.so.6 (0x00007f2d09a00000)
    │ │ │ └─ld-linux-x86-64.so.2 => /lib64/ld-linux-x86-64.so.2 (0x00007f2d0a19d000)
    │ │ └─ld-linux-x86-64.so.2 => /lib64/ld-linux-x86-64.so.2 (0x00007f2d0a19d000)
    │ ├─libc.so.6 => /lib64/libc.so.6 (0x00007f2d09a00000)
    │ │ └─ld-linux-x86-64.so.2 => /lib64/ld-linux-x86-64.so.2 (0x00007f2d0a19d000)
    │ ├─ld-linux-x86-64.so.2 => /lib64/ld-linux-x86-64.so.2 (0x00007f2d0a19d000)
    │ └─libgcc_s.so.1 => /lib64/libgcc_s.so.1 (0x00007f2d0a07b000)
    │   └─libc.so.6 => /lib64/libc.so.6 (0x00007f2d09a00000)
    │     └─ld-linux-x86-64.so.2 => /lib64/ld-linux-x86-64.so.2 (0x00007f2d0a19d000)
    ├─libm.so.6 => /lib64/libm.so.6 (0x00007f2d0a096000)
    │ ├─libc.so.6 => /lib64/libc.so.6 (0x00007f2d09a00000)
    │ │ └─ld-linux-x86-64.so.2 => /lib64/ld-linux-x86-64.so.2 (0x00007f2d0a19d000)
    │ └─ld-linux-x86-64.so.2 => /lib64/ld-linux-x86-64.so.2 (0x00007f2d0a19d000)
    ├─libgcc_s.so.1 => /lib64/libgcc_s.so.1 (0x00007f2d0a07b000)
    │ └─libc.so.6 => /lib64/libc.so.6 (0x00007f2d09a00000)
    │   └─ld-linux-x86-64.so.2 => /lib64/ld-linux-x86-64.so.2 (0x00007f2d0a19d000)
    └─libc.so.6 => /lib64/libc.so.6 (0x00007f2d09a00000)
      └─ld-linux-x86-64.so.2 => /lib64/ld-linux-x86-64.so.2 (0x00007f2d0a19d000)


[pstream.h]: https://sourceforge.net/p/pstreams/code/ci/master/tree/pstream.h?format=raw
[ldd]: http://man7.org/linux/man-pages/man1/ldd.1.html
